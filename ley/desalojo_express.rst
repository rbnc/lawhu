.. desalojo_express file 

Ley de desalojo express
=======================

.. toctree::
   :maxdepth: 3
   :numbered: 


Ley que regula el procedimiento especial de desalojo con intervención notarial
LEY Nº 30933

Artículo 1. Objeto de la ley
############################

La presente ley tiene por objeto establecer y regular el procedimiento especial de desalojo mediante la intervención de notario y con ejecución judicial.

Artículo 2. Ámbito de aplicación de la Ley
##########################################

Podrán acogerse a lo dispuesto en la presente ley el propietario, el arrendador, el administrador y todo aquel que considere tener derecho a la restitución de un bien inmueble, contra el arrendatario que se ha sometido expresamente al procedimiento establecido por la presente ley.

No están comprendidos en el ámbito de la presente ley la desocupación por motivo de contratos de alquiler venta, arrendamiento financiero u otros tipos de contratos que incluyan pago para adquirir la propiedad del inmueble. (*)

(*) Artículo modificado por la Décima Disposición Complementaria Modificatoria del Decreto de Urgencia N° 013-2020, publicado el 23 enero 2020, cuyo texto es el siguiente:

“Artículo 2. Ámbito de aplicación de la Ley

Podrán acogerse a lo dispuesto en la presente ley el propietario, el arrendador, el administrador y todo aquel que considere tener derecho a la restitución de un bien inmueble, contra el arrendatario que se ha sometido expresamente al procedimiento establecido por la presente ley.

No están comprendidos en el ámbito de la presente ley la desocupación por motivo de contratos de alquiler venta u otros tipos de contratos que incluyan pago para adquirir la propiedad del inmueble, a excepción de los contratos de arrendamiento financiero.”

Artículo 3. Competencia
#######################

Son competentes para llevar a cabo el procedimiento especial establecido en la presente ley:

1. Los notarios, dentro de la provincia en la que se ubica el bien inmueble arrendado, para la constatación de las causales de desalojo.

2. El juez de paz letrado del distrito en el que se ubica el bien inmueble arrendado, para ordenar y ejecutar el lanzamiento.

Artículo 4. Requisitos de procedibilidad
########################################

Para solicitar el desalojo de un bien inmueble al amparo de la presente ley, se debe cumplir con los siguientes requisitos:

1. El inmueble materia de desalojo notarial debe encontrarse individualizado de manera inequívoca; y en el contrato de arrendamiento debe consignarse las referencias precisas de su ubicación.

2. El contrato de arrendamiento debe estar contenido en el Formulario Único de Arrendamiento de Inmueble destinado a Vivienda (FUA), creado por el Decreto Legislativo 1177, Decreto Legislativo que establece el Régimen de Promoción del Arrendamiento para Vivienda; o en escritura pública. En este caso, el contrato de arrendamiento puede estar destinado a vivienda, comercio, industria u otros fines.

3. Las modificaciones o adendas al contrato de arrendamiento deben cumplir con la misma formalidad que el contrato primigenio.

Artículo 5. Contenido del contrato de arrendamiento
###################################################

El contrato de arrendamiento a que se refiere el artículo 4 debe:

1. Contener una cláusula de allanamiento a futuro, del arrendatario para la restitución del bien inmueble por vencimiento del plazo de contrato o la resolución del arrendamiento por falta de pago de la renta.

2. Contener una cláusula de sometimiento expreso a lo establecido por la presente ley para que el notario constate las causales de vencimiento del plazo del contrato o la resolución por falta de pago de la renta, y el juez de paz letrado ordene y ejecute el desalojo.

La cláusula de sometimiento expreso a la presente ley contiene de manera expresa e indubitable la decisión de las partes, que, ante la configuración de cualquiera de las causales de desalojo previstas en la presente ley, se someten a la competencia del notario para la constatación de dicha causal y la ejecución del desalojo por parte del juez de paz letrado.

3. Consignar el número, tipo y moneda de la cuenta de abono abierta en una empresa del sistema financiero o en una cooperativa de ahorro y crédito supervisada por la Superintendencia de Banca, Seguros y Administradoras Privadas de Fondos de Pensiones (SBS), para que el arrendatario abone la renta convenida en el contrato de arrendamiento.

En caso de que la cuenta de abono sea modificada, el arrendador pone en conocimiento al arrendatario dicha situación mediante comunicación de fecha cierta. Mientras no se efectúe dicha comunicación, los pagos realizados en la cuenta primigenia son considerados válidos.

Artículo 6. Requisitos de la solicitud
######################################

6.1 La solicitud de desalojo se presenta por escrito, señalando el nombre del propietario o el de aquel que tenga derecho a la restitución del bien inmueble, su domicilio, documento de identidad y firma; así como, el nombre del arrendatario, su domicilio contractual, de ser el caso, su número o copia del documento de identidad.

6.2 A la solicitud de desalojo se adjuntan los siguientes documentos:

1. El original o la copia legalizada del formulario FUA o de la escritura pública del contrato de arrendamiento, el cual debe cumplir con los requisitos establecidos en los artículos 4 y 5 de la presente ley.

2. El original o la copia legalizada de la carta notarial cursada al arrendatario en el inmueble materia de desalojo y a su domicilio contractual, de ser el caso; mediante ella se requiere la restitución del bien inmueble por el vencimiento del plazo o por la resolución del contrato por falta de pago, según corresponda.

6.3 La solicitud de desalojo solo puede ampararse en las causales establecidas en el artículo 7 de la presente ley. Las partes pueden recurrir ante las autoridades competentes, a efectos de exigir el cumplimiento de las demás obligaciones derivadas del contrato de arrendamiento.

Artículo 7. Causales
####################

El desalojo al amparo de la presente ley procede cuando se configura alguna de las siguientes causales:

1. Vencimiento del plazo del contrato de arrendamiento; o,

2. Incumplimiento del pago de la renta convenida de acuerdo a lo establecido en el contrato de arrendamiento. Si no se ha establecido plazo alguno, se aplica lo dispuesto en el inciso 1 del artículo 1697 del Código Civil.

A efectos de constatar el incumplimiento del pago de la renta, el notario solo considera la constancia de transferencia o depósito de los pagos realizados a través de la cuenta de abono acordada por las partes, conforme lo establece el inciso 3 del artículo 5 de la presente ley, hasta antes de la recepción de la carta notarial a la que se refiere el inciso 2 del artículo 6.2 de la presente ley.

Artículo 8. Procedimiento ante notario para la constatación de las causales de desalojo
#######################################################################################

8.1 El notario recibe la solicitud de desalojo, constata el cumplimiento de los requisitos de procedibilidad, el contenido del contrato de arrendamiento y los requisitos de la solicitud establecidos por la presente ley.

8.2 El notario notifica al arrendatario en el inmueble materia de desalojo y en su domicilio contractual, de ser el caso, para que, dentro del plazo de cinco (5) días hábiles de recibida la comunicación, acredite no estar incurso en alguna de las causales señaladas en el artículo 7.

8.3 El arrendatario sólo puede formular oposición sustentada en:

a) La renovación o prórroga del plazo del contrato de arrendamiento con las mismas formalidades que el contrato primigenio;

b) La constancia de transferencia o depósito de los pagos de la renta realizados en la cuenta de abono acordada por las partes;

c) El incumplimiento de las formalidades establecidas por la presente ley.

8.4 El notario, con la respuesta del arrendatario presentada dentro del plazo señalado en el numeral 8.2 del presente artículo, constata si se configura alguna de las causales de desalojo previstas en el artículo 7 de la presente ley, en cuyo caso realiza las siguientes acciones:

1. Extiende un acta no contenciosa dejando constancia fehaciente e indubitable del vencimiento del contrato o de la resolución del mismo por falta de pago, con la declaración de la procedencia del desalojo, lo cual protocoliza en el Registro Notarial de Asuntos No Contenciosos. El acta extendida por el notario constituye título ejecutivo especial para proceder sin más trámite al lanzamiento del inmueble conforme al artículo 9.

2. Remite la copia legalizada del expediente al juez de paz letrado del distrito en el cual se ubica el bien inmueble arrendado, a efectos de que proceda conforme a lo establecido en el artículo 9.

8.5 El trámite de desalojo notarial finaliza si:

1. No se configura alguna de las causales de desalojo previstas en el artículo 7, en dicho caso el notario finaliza el trámite comunicando de este hecho al solicitante.

2. Las partes acuerdan su finalización, en cualquier momento del procedimiento. En este caso, el notario levanta el acta no contenciosa correspondiente concluyendo el trámite.

Artículo 9. Trámite judicial de lanzamiento
###########################################

9.1 Culminada la etapa notarial del procedimiento especial a que se refiere la presente ley, el interesado formula solicitud de lanzamiento dirigida al juez de paz letrado competente, con la autorización de letrado y el pago de la tasa judicial respectiva, para que sea trasladada por el notario conjuntamente con la copia legalizada del expediente notarial.

9.2 Dentro del plazo de tres (3) días hábiles de recibidas la solicitud del interesado y las copias legalizadas del expediente remitidas por el notario, el juez de paz letrado competente verifica los requisitos de la solicitud y emite la resolución judicial en la que dispone el lanzamiento contra el arrendatario o contra quien se encuentre en el inmueble; así como, la orden de descerraje en caso de resistencia al cumplimiento del mandato judicial o de encontrarse cerrado el inmueble. La resolución judicial es impugnable sin efecto suspensivo.

9.3 El juez de paz letrado cursa oficio a la dependencia correspondiente de la Policía Nacional del Perú (PNP), para que en el plazo de dos (2) días hábiles contados desde el día siguiente de la notificación, obligatoriamente y bajo responsabilidad, preste asistencia y garantía para la ejecución del desalojo en la forma y plazo indicados en su resolución judicial.

9.4 Culminado el trámite de lanzamiento, el interesado podrá solicitar ante el mismo juez de paz letrado el pago de costas y costos del proceso, así como el de los servicios notariales derivados del desalojo, conforme a lo dispuesto en los artículos 417 y 419 del Código Procesal Civil en lo que no se oponga a la presente ley.

DISPOSICIONES COMPLEMENTARIAS FINALES
#####################################

PRIMERA. Aplicación supletoria del Código Civil, del Código Procesal Civil y de la Ley 26662, Ley de Competencia Notarial en Asuntos No Contenciosos

En todo lo no regulado en la presente ley se aplican supletoriamente las disposiciones del Código Civil, del Código Procesal Civil, y de la Ley 26662, Ley de Competencia Notarial en Asuntos No Contenciosos, según corresponda.

SEGUNDA. Libre elección del notario

El solicitante tiene el derecho a tramitar el procedimiento de desalojo con intervención notarial, ante el notario de su libre elección de cualquier distrito dentro de la provincia en la que se encuentra el inmueble materia de desalojo, conforme a la competencia establecida en el numeral 1 del artículo 3 de la presente ley.

TERCERA. Modificación del artículo 58 del Decreto Legislativo 1049, Decreto Legislativo del Notariado

Modifícanse los literales k) y l) del artículo 58 del Decreto Legislativo 1049, Decreto Legislativo del Notariado, con el siguiente texto:

“Artículo 58.- Inexigencia de la minuta

No será exigible la minuta en los actos siguientes:

[...]

k) Arrendamiento de inmuebles sujetos a la Ley que regula el procedimiento especial de desalojo con intervención notarial.

l) Otros que la ley señale”.

DISPOSICIÓN COMPLEMENTARIA TRANSITORIA

ÚNICA. Contratos de arrendamiento anteriores a la vigencia de la presente ley

Los contratos de arrendamiento que se celebraron en el marco del Decreto Legislativo 1177, Decreto Legislativo que establece el Régimen de Promoción del Arrendamiento para Vivienda, y los anteriores a este podrán acogerse a la presente ley, siempre y cuando suscriban una adenda que cumpla con las mismas formalidades del contrato de arrendamiento establecidas en la presente ley.

Comuníquese al señor Presidente de la República para su promulgación.

En Lima, a los dieciséis días del mes de abril de dos mil diecinueve.

DANIEL SALAVERRY VILLA

Presidente del Congreso de la República

LEYLA CHIHUÁN RAMOS

Primera Vicepresidenta del Congreso de la República

AL SEÑOR PRESIDENTE DE LA REPÚBLICA

POR TANTO:

Mando se publique y cumpla.

Dado en la Casa de Gobierno, en Lima, a los veintitrés días del mes de abril del año dos mil diecinueve.

MARTÍN ALBERTO VIZCARRA CORNEJO

Presidente de la República

SALVADOR DEL SOLAR LABARTHE

Presidente del Consejo de Ministros


