.. lawhu documentation master file, created by
   sphinx-quickstart on Sat May 23 16:19:07 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Lawhu's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ley/desalojo_express
   ley/sirejud-20200521
   ley/ley-26389

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
